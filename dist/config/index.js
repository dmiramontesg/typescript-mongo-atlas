"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config();
const ConfigEnv = () => {
    return {
        //   NODE_ENV: process.env.NODE_ENV,
        //   PORT: process.env.PORT ? Number(process.env.PORT) : undefined,
        MONGO_URI: process.env.MONGO_DB_URI
    };
};
const getConfig = (config) => {
    for (const [key, value] of Object.entries(config)) {
        if (value === undefined) {
            throw new Error(`Missing key ${key} in index.env`);
        }
    }
    return config;
};
const config = ConfigEnv();
const keyConfig = getConfig(config);
exports.default = keyConfig;
