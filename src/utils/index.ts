import config from "../config";
import mongoose from "mongoose";


const connectDB = async () =>{
    const connection = await mongoose.connect(config.MONGO_URI);
    console.log(`Mongo db conected:`,connection.connection.host);
};

export default connectDB;