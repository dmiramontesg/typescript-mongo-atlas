import dotenv from 'dotenv';
dotenv.config();

interface ENV {
 //   NODE_ENV: string | undefined;
 //   PORT: number | undefined;
    MONGO_URI: string | undefined;
}

interface Config {
 //   NODE_ENV: string;
 //   PORT: number;
    MONGO_URI: string;
}


const ConfigEnv = (): ENV =>{
  return {
 //   NODE_ENV: process.env.NODE_ENV,
 //   PORT: process.env.PORT ? Number(process.env.PORT) : undefined,
    MONGO_URI: process.env.MONGO_DB_URI
    }
}

const getConfig = (config:ENV):Config =>{
    for(const[key,value] of Object.entries(config)){
        if(value === undefined){
            throw new Error(`Missing key ${key} in index.env`)
        }
    }
    return config as Config;
}

const config = ConfigEnv();
const keyConfig = getConfig(config);


export default keyConfig;